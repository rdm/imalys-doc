*[ASTER]: ASTER
*[ENVI]:
*[EPSG]: EPSG
*[ESRI]:
*[EVI]:
*[GDAL]: 
*[GIS]: Geographic Information System
*[IDM]: Inverse Difference Moment
*[L2SP]: 
*[LAI]: Leaf Area Index
*[NDVI]: Normalized Difference Vegetation Index
*[NIRv]: Near-Infrared Reflectance of Vegetation
*[OLI]: OLI
*[PAR]:
*[ROI]: Region of Interest
*[SRTM]: SRTM
*[TOA]: Top of Atmosphere
*[UTM]:
*[WGS]:
*[WKT]:
