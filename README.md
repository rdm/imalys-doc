# Documentation for the tool Imalys – Image Analysis

*Imalys* is a software library for landscape analysis based on satellite images. The library provides tools to select, extract, transform and combine raster and vector data. Image quality, landscape diversity, change and different landuse types can be analyzed in time and space. Landuse borders can be delineated and typical landscape structures can be characterized by a self adjusting process.

This is the repository for our Imalys online [documentation](https://rdm.pages.ufz.de/imalys-doc).

[Here](https://codebase.helmholtz.cloud/esis/Imalys) you can find the [Imalys code repository](https://codebase.helmholtz.cloud/esis/Imalys).

## Contributing

Please refer to our [contributing guidelines](CONTRIBUTING.md) to see how you can contribute to *Imalys documentation*.

## User support

If you need help or have a question, you can use the Imalys user support: [imalys-support@ufz.de](mailto:imalys-support@ufz.de)

## Copyright and License

Copyright(c) 2023, [Helmholtz-Zentrum für Umweltforschung GmbH -- UFZ](https://www.ufz.de). All rights reserved.

The documentation is distributed under a [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.

## Acknowledgements

This project was made possible by the [Research Data Management Team (RDM)](https://www.ufz.de/index.php?en=45348) at the [Helmholtz Center for Environmental Research - UFZ](https://www.ufz.de).

-----------------

<a href="https://www.ufz.de/index.php?en=33573">
    <img src="https://git.ufz.de/rdm-software/saqc/raw/develop/docs/resources/images/representative/UFZLogo.png" width="400"/>
</a>

<a href="https://www.ufz.de/index.php?en=45348">
    <img src="https://git.ufz.de/rdm-software/saqc/raw/develop/docs/resources/images/representative/RDMLogo.png" align="right" width="220"/>
</a>
