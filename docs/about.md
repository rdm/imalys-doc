# About

*Imalys* is a software library for landscape analysis based on satellite images.
[Here](https://codebase.helmholtz.cloud/esis/Imalys) you can find the [Imalys
code repository](https://codebase.helmholtz.cloud/esis/Imalys).

## License

Imalys is distributed under a
[GPLv3](https://codebase.helmholtz.cloud/esis/Imalys/-/blob/1123c366c8b164553bebf496e90e9556acfc8fc5/LICENSE.md)
license.

## Acknowledgements

The Software Imalys and the documentation was made possible by the [Research
Data Management Team (RDM)](https://www.ufz.de/index.php?en=45348) at the
[Helmholtz Center for Environmental Research - UFZ](https://www.ufz.de).

-----------------

<a href="https://www.ufz.de/index.php?en=33573">
    <img src="https://git.ufz.de/rdm-software/saqc/raw/develop/docs/resources/images/representative/UFZLogo.png" width="260"/>
</a>
<a href="https://www.ufz.de/index.php?en=45348">
    <img src="https://git.ufz.de/rdm-software/saqc/raw/develop/docs/resources/images/representative/RDMLogo.png" align="right" width="200"/>
</a>