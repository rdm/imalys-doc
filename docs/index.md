# Imalys - Image Analysis

[Imalys](https://codebase.helmholtz.cloud/esis/Imalys/) is a collection of software tools
to organize and analyse remote sensing images. The source code can be found
[here](https://codebase.helmholtz.cloud/esis/Imalys/). The tools are closely
related to traits. Imalys is designed to run on a
server like the [GeoServer](https://geoserver.org/). Imalys is called as a
command via a terminal. A text file with the process chain must be passed
together with the command. A second text file with a list of input values can be
given to repeat the process chain for different times or places.

Imalys consists of five modules:

!!! info inline end "Imalys Modules"

     - Imalys provides processes to import and export raster and vector data. Different raster and vector formats can be used.
     - Between import and export, there are tools to analyze individual pixels, extract textures, delineate land-use borders and classify the image content.
     - All processes use the same interface, but are logically interdependent.

![Imalys Modules Zones](_img/imalys_modules.png){width="350"}

## Modules 

### Import

The “import” module is responsible to select and extract images from the
compressed archives of the providers, choose appropriate bands, cut out a
predefined region of interest, calibrate the raw values to reflectance or
radiation, reproject the image if necessary and merge different parts if the
region if they are dissipated over different tiles. Import returns a seamless
and calibrated image for a given region and time.

### Pixel

The “pixel” module provides tools to reduce image stacks using statistical
methods like the median or variance, create timelines and extract trends or
build eigenvalues. Indices like the NDVI are available. The second part of the
“pixel” module are tools to collect information about the local environment
(vicinity) of each pixel using a kernel. Kernels can increase image contrast
like a Laplace transformation does or extract local patterns like Rao’s
Diversity.

### Zones

Seamless aggregated pixels with roughly the same properties will be called
“zones”. Zones follows the OBIA concept [^1]. Imalys provides tools to create
such zones and in a second step aggregate zones to larger “objects”, that are
characterized by the spatial patterns of the different zones they include. Using
zones, images can be depicted by both pixels or polygons.

### Mapping

Imalys provides classification tools for pixels, zones and objects. All of them
are self calibrating pattern detectors that can work without any training. The
“mapping” module provides additional tools to use training samples, compare
classification results with given maps and track outliers in space and time.

### Export

The “export” module is responsible to store the results in a chosen vector or
raster format at a different place. Zones and classes can be exported as images
or maps. To use the results with other applications a couple of transfer tools
are available.

## Control

Imalys is controlled by commands and parameters. Both can be combined to process
chains to get results without manual interaction. Process chains can use
variables to repeat a given process for a list of different input sets.

The following pages give an overview how to use Imalys. Commands and their
parameters are explained in detail. In the [tutorial](tutorials/index.md) section we describe usage examples of Imalys. The [background](background/index.md) articles explain important algorithms used with Imalys.

[^1]: {==(Blaschke, 2014)==}
