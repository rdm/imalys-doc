# Background

## Delineate Zones

Imalys creates [zones](../process-definitions/zones.md) with an iterated
watershed process. The first step merges the most similar pixel neighbors by
linking them with a common ID.

A threshold can suppress the merge. The threshold can be influenced by the user.

!!! info inline end "Create New Zones"

     - Extremely enlarged pixels and their grayscales are shown
     - **Above**: Maximum similarity between seven pixel pairs ⇒ 7 borders (black lines) are deleted
     - **Bottom**: Maximum similarity in 3 cases ⇒ two borders (black lines) are deleted, one border (white line) between two larger areas is retained

![Delineate Zones](../_img/delineate_zones.png){ width="450" }

As “threshold”, Imalys uses the first principal component of all normalized
brightness modulations (a-b) / (a+b) with a,b as the brightness values.
Normalization promotes differentiation in the dark areas of the image. The
principal component promotes the effect of contrasts that only occur in one
band. After the first iteration, which only combines individual pixels into
first zones, the process combines existing zones into larger ones. Again, only
the locally most similar zones are merged and again the above threshold must be
adhered to. In addition, the increasing size of the zones acts as an inhibiting
factor when merging zones. The zones grow over the course of repeated steps
until the process no longer finds suitable precursors.

[Index](../process-definitions/index.md) uses an exponential function of the
zone size to inhibit the merge. The exponent can be selected. Small exponents
(close to zero) reduce the influence of the size of a possible merge. This
creates zones that are mainly controlled by contrast. They are largely
homogeneous in spectral terms and can become very large. Edges are better
recognized. Large exponents (close to one) lead to zones with smaller size
differences. The spectral differences within the zones are larger.

The combination of brightness and area is necessary to create zones with
reasonable areas. A threshold for only one parameter would separate the image
into individual pixels and “infinitely” large zones.

## Mapping

### Kohonen

The basis of the mapping is the Euclidean distance in the n-dimensional feature
space, similar to the IsoClass method. Each neuron represents a separate class.
According to Kohonen’s suggestion[^1], the neurons have individual properties,
not only connections as neurons of the perceptron type would have. One of the
individual properties is a receptive field, a section of the feature space in
which the neurons can recognize features. As for the rest, they are blind. This
property enhances their ability to depict small but common differences.

### Model

In terms of zones two abstractions take place. One of them are spectral
features. Zones have a spectral composition like pixels but the value is the
mean of all pixels connected to one zone. Local differences and texture might be
lost. On the other hand the class definition gains information from the size,
shape and connection of the zones. A specific color combination in a small zone
might have a very different meaning than in a large zone.

### Objects

Objects ([Fabric](../process-definitions/fabric.md)) are an extreme abstraction
of this enhancement. The class definition only depends on the frequency of
borders between different zone classes. The zone classes are only defined by
their spectral composition. The form and size is carried out by the connections
between the zones. The border length is counted as pixel borders. This includes
all pixels, and even pixel borders within one zone. This principle introduces
the necessary plasticity in the object definition.

Objects can consist of simple patterns that are repeated over a large area but
depicted by small zones. Objects can also consist of one large but homogeneous
zone. In the first instance, the object is defined mainly by the connections
between zones, in the second instance, internal borders of the large area
dominate the definition and the connections play a minor role (see sections on
Object Generation). In practice there is a smooth transition between both
extremes.

Since the object class definition only depends on different frequencies, each
zone class may occur in each object class. Non-specific zone classes such as
“shadows” can be defined in all object classes. They are not characteristic for
the object, but make it complete.

## Object Generation

[Fabric](../process-definitions/fabric.md) combines two processes in three
steps. Pixels are combined to form zones, which are classified by means of their
spectral features and finally classified zones are combined to form objects with
specific spatial combinations of different zones.

!!! info inline end "Image Objects"

     - Image objects are created in three steps:
     - **(1)** Pixels of similar spectral combination are combined into zones
     - **(2)** Zones are classified into clusters with typical spectral combinations (types)
     - **(3)** Types are aggregated to form objects according to their connections with other zones
     - Each zone type can be part of each object definition

![Object Definition: Three
Zones](../_img/object_generation.png){width="450"}

### Spatial features

Images of the earth’s surface are structured. Pixels with almost the same
spectral combination are not randomly distributed but form clusters and
sometimes regular patterns. The [Index](../process-definitions/index.md) process
combines regions with pixels of almost the same spectral combination into
[zones](../process-definitions/zones.md). Zones have spectral features like
pixels.

!!! info inline "Object Definition: 3 Zones and 17 Contacts"

     - An object consisting of **three zones** (red-green-blue) with a total of **12** pixels is characterized by **17** contacts (yellow symbols). 
     - The **object definition** is based solely on the **nature** and **frequency** of these **contacts**.

![Object Definition: Three
Zones](../_img/object_definition_3_zones.png){width="300"}

![Object Definition:
Contacts](../_img/object_definition_contacts.png){width="300"}

### Combination

[Fabric](../process-definitions/fabric.md) registers all class combinations
between two pixels for each zone. “All” also includes pixels within the zone.
The frequency of the class combinations at all borders between two pixels form a
matrix that can be used for a second level classification.
[Fabric](../process-definitions/fabric.md) arranges them almost the same as
spectral classes ([Mapping](../process-definitions/mapping.md)).

### Examples

A typical result is for objects to be dominated by one large zone whereas the
connected zones are small and unspecific. Water bodies or agricultural areas
require a class definition of this kind. The other extreme is represented by
small zones that only have contact with a few other classes. The class
definition is dominated by their connections. They reproduce small-scale
patterns that can be repeated over a large area. The definition will combine
zones with different spectral compositions but similar connections, thus
defining regular patterns. Object classes of this type are typically found in
settlements or forests.

Since the object definition only uses frequencies there is a smooth transition
between small-scale patterns and large zones with unspecific connections.

### Spectral features

During the second step, the [zones](../process-definitions/zones.md) process
classifies the characteristics of individual zones and assigns zone classes. In
this case the classification only depends on the spectral characteristics of the
zones and the normalized texture (```Normal```) between zonal pixels. After the
second step, zones map the spatial distribution of image features and the zone
classes map the spectral distribution.
[Fabric](../process-definitions/fabric.md) combines both to a class definition
that includes size, shape and typical patterns of visible structures in the
image.

[^1]: {==Kohonen 1982==}