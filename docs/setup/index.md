# Setup

Imalys source code and executables are available in the [Helmholtz
Codebase](https://codebase.helmholtz.cloud/esis/Imalys).

## xImalys 

All commands and processes of Imalys are combined into an executable program
```xImalys```. ```xImalys``` does not need to be installed. It is sufficient to
copy the file to ```usr/local/bin``` or each other directory you may prefer.
- Select the “binaries” page and use the download button [↓] to copy the binary
files as an archive to your disk.
- Extract the contents of the archive
- Copy the binary files to your ```usr/local/bin``` directory. The directory
should have write permissions to store runtime data.

```bash
sudo cp »source«/x_Imalys usr/local/bin 
sudo cp »source«/r_Imalys usr/local/bin
cp »source«/tutorial ~/tutorial
```

This will copy the binaries to your ```usr/local/bin``` directory and the
tutorial files to your home directory. You may copy the tutorial to each
directory with writing permission. ```»source«``` is the directory where you
stored the extracted files from the GitLab page. ```usr/local/bin``` is
protected. You will need administrative rights. ```usr/local/bin``` is not
included into the package system.

## GDAL library

Imalys uses the [GDAL library](https://github.com/OSGeo/GDAL) of the [Open
Source Geospatial Foundation](https://www.osgeo.org) for a lot of processes.
This library must be installed under ```/usr/bin```. In many Linux distributions
this is the case. Alternatively GDAL can be installed from GitHub:
[https://github.com/OSGeo/GDAL](https://github.com/OSGeo/GDAL).

## Command

The executable ```xImalys``` must be called as a command in a shell or terminal.
Imalys must be called with a parameter containing a process chain (hook). A
second parameter for variables in the process chain is optional.

```bash
/usr/local/bin/xImalys »path_to_process-chain« 
/usr/local/bin/rImalys »path_to_process-chain« »path_to_variable-list«
```

This call will run ```rImalys``` with the process chain
```path_to_process-chain``` and the variable list ```path_to_variable-list```.
Both parameters must be given as full pathname.
