# ESIS

The Ecosystem Integrity Service (ESIS) was designed to link, analyze and model
data from the natural environment. Currently, land-use borders, landscape
diversity, development and the main types of landcover are the main objectives.

![Object Definition: Three
Zones](../_img/vegetation_index_europe.png){width="800"}

!!! info "Vegetation index of Central Europe"

     - Vegetation index of Central Europe superimposed with shading from an elevation model
     - Elevation data from SRTM mission (2001), image data Landsat-8, 2014-2020
     - Vegetation index NIRv (red and infrared). Values between 0.0 (turquoise), 0.1 (red brown) to 0.4 (dark green) 

## Traits

In ESIS, **traits** are well-defined landscape features associated with a specific
place and time. Traits should be scale-invariant, sensor-independent and
globally applicable. Each location can be described by numerous traits.

## Imalys

The software library **Imalys (Image Analysis)** was developed to derive traits
from satellite images. Imalys offers generic processes for specific tasks,
wrappers for known processes and a workflow that can be automated.

## Tutorials

The [tutorials](../tutorials/index.md) describe the application of the processes in Imalys and
provide tips on how to link them. The library includes executable examples that
can serve as a starting point for own solutions.

## Methods

Some traits provide new insights into the structure of a landscape, others use
new software techniques. The background, methods and motivation of the most
important processes are described under [Process
Definitions](../process-definitions/index.md).
