# [T7] Process Chain Variables

A ```$``` sign followed by a single number will be interpreted as a variable.
Variables must be defined at the beginning of the process chain and can be used
for different purposes:

1. Long filenames can be substituted by a short expression
   ([T7a](#t7a-process-chain-variables))
2. All input parameters for a given process chain can be concentrated at the
   beginning of the process chain ([T7a](#t7a-process-chain-variables))
3. Appropriate image archives can be selected using a
   [catalog](../process-definitions/catalog.md)
   ({==[T7?](#t7-process-chain-variables)==})
4. A list of parameter sets can be given to repeat a process chain for different
   input data without manual interaction ([T7b](#t7b-repetition))

## [T7a] Process chain variables

 The process chain can use variables instead of parameters. The variables must
 be defined at the beginning of the process chain
 ([replace](../process-definitions/replace.md)). Each occurance of the variable
 will be exchanged by the defined string.

```bash
IMALYS [replace]
replace
    $1 = c7106
    $2 = Leipzig
home
    directory = »user«/.imalys
    clear = true
    log = »user«/results/$1_$2
```

will replace the last line to

```bash
 ...
 log = »user«/results/c7106_Leipzig
```

This [replace](../process-definitions/replace.md) command will translate the
last line in the example as shown above. Each variable of the whole process
chain is replaced in the same way.

## [T7b] Repetition

 Variables can also be used to repeat a whole process chain with different
 inputs and produce corresponding outputs without further interaction. In this
 case Imalys has to be called with two parameters, the process chain and a
 variable table ([repeat](../process-definitions/repeat.md))

```bash
IMALYS [repeat]
...
catalog
    archives = »user«/archives/**L2SP**
import
    database = »user«/archives/tilecenter.csv
    distance = 1.00
    frame = »user«/frames/$1.gpkg
    quality = 0.86
    bands = _B2, _B3, _B4, _B5, _B6, _B7
    factor = 2.75e-5
    offset = -0.2
reduce
    select = import
    execute = bestof
export
    select = bestof
    target = »user«/results/$1_$2.tif

c3542 Berlin-West
c3546 Berlin-Mite
c4738 Leipzig
c6730 Nürnberg
```

To repeat tutorial 7b for different maps, the TK100 frames and the result names
must be given by variables. The imported images are reduced to one median image
(```bestof```) and exported as GEO-Tiff with the name of the map and the city
(```$1_$2.tif```).

```Database``` includes all Landsat archives in the given directory. ```Distance
= 1``` acts as a preselection. Only archives that reach the center of the given
```frame``` will be extracted. The second step of the selection is done by
```quality = 0.86```. The parameter will limit the cloud cover to a maximum of
14% but will also reject tiles with less than 86% of usable pixels within the
```frame```.

The variable table only contains the values. All variables for one run of the
process chain must be given in one line.  Each line will induce another run of
the process chain. The variables are divided by tabs. The columns represent the
numbers of the variables.

In this case the process chain will be repeated for the cities of Berlin-West,
Berlin-Mitte, Leipzig and Nürnberg.

## [T7d] Combine database, filter and repetition

A process chain can include repeated processes. In this case the archives of
different cities are extracted using a “database”  and the result is combined to
two images of each city.

```bash

IMALYS [repeat]
...
import
    database = »user«/archives_2011/tilecenter.csv
    distance = 1.00
    frame = »user«/frames/$1.gpkg
    quality = 0.86
    bands = _B2, _B3, _B4, _B5, _B6, _B7
    factor = 2.75e-5
    offset = -0.2
    warp = 32632
    pixel = 30
compile
    period = 20110501 - 20110730
reduce
    select = compile
    execute = bestof
export
    select = bestof
    target = »user«/results/$1_$2_May-July.tif
compile
    period = 20110801 - 20111031
reduce
    select = compile
    execute = bestof
export
    select = bestof
    target = »user«/results/$1_$2_Aug-Oct.tif
```
