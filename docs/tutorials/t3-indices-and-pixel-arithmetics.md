# [T3] Indices and Pixel Arithmetics

The ratio of two, three or on rare occasions four bands is traditionally used as
an indicator for features that are not directly visible. The most well-known are
the vegetation indices (NDVI, EVI, LAI, ...). These indices are based on the
reflections at different frequencies (bands) and can be applied to individual
pixels.

![Vegetation Index](../_img/NIRv_HohesHolz.png){width=800px}

!!! info "Vegetation Index"
     - Near Infrared Vegetation (NIRv)
     - Narrow black lines mark boundaries of land use (zones).
    - The NIRv values range between 0.0 (turquoise) and 0.4 (dark green)
    - “Hohes Holz” and “Großer Bruch” in the Bode catchment area, Landsat-8, First half of the growing season
     2014-2020

## T3a Vegetation indices

Imalys has implemented two vegetation indices, one for the metabolic rate of
green plants (NIRv) and one for the relative coverage of the landscape with
green leaves (LAI). The user should take into account that all vegetation
indices are approximations. They assume spectral dependencies and also depend on
the type of sensor.

### Pseudocolor

A Quantum GIS color palette for NirV is provided at
```/tutorial/visual/NirV.qml```. To use this palette, select the properties menu
with ```[Layer][Layer Properties]```, set the min and max value to 0.0 and 0.4
respectively and select the ```NirV.qml``` palette with the ```[Style][Load
Style]``` command at the properties menu.

In addition to special data sources, there are numerous other methods to derive
proxies for features such as soil moisture from the visible image data.

```bash
IMALYS [tutorial 3a]
home
	directory=»user«/.imalys
	clear=false
	log=»tutorial«/results
compile
	period=20220501-20220731
reduce
	select=compile
	execute=bestof
reduce
	select=bestof
	execute=NirV
	execute=NDVI
	red=2
	nir=3
export
	select=NirV
	target=»tutorial«/results/NirV_20220501-20220731.tif
export
	select=NDVI
	target=»tutorial«/results/NDVI_20220501-20220731.tif

# »user« must be exchanged with the home directory of the user
# »tutorial« must be exchanged with the path of the tutorial
```

The results of [tutorial2d](../tutorials/t2-seamless-image-products.md#t2d-import-with-an-image-catalog)
must be available at the working directory

### Workflow

Tutorial 3a demonstrates how to rearrange intermediate results to two different
pixel indices. The [compile](../process-definitions/compile.md) process must be
repeated to select images of the first half of the vegetation period.
[ceduce](../process-definitions/reduce.md) creates an optimized 6 band image.

The second [reduce](../process-definitions/reduce.md) calculates the vegetation
index “NDVI” and the plant metabolism index “NirV”. To calculate the indices,
the position of the red and the near infrared bands within the image must be
given as a number, starting with zero for the upmost layer (green). If the input
images are identical, the [reduce](../process-definitions/reduce.md) commands
can be concatenated. Two [export](../process-definitions/export.md) commands
save the results as Geo-Tiff files in the ```results``` directory.

### Altered time period

Only different months given under [compile](../process-definitions/compile.md)
and [export](../process-definitions/export.md) can create the same indices for
the second half of the vegetation period 2022.

## [T3b] Time series and change

Image data from remote sensing are snapshots that show arbitrary differences.
All tutorials above use a short time course to return “typical” image values for
the given period (see [median](t2-seamless-image-products.md#median)). Sentinel
2 and Landsat 8 have been in operation since 2016 and 2014 respectively.
Typically 1 to 3 usable images are available for three months. Landsat sensors
(TM and OLI) allow time series over 40 years.

![Variance Over 7 Years](../_img/variance_over_7_years.png){width=800px}

!!! info "Variance Over 7 Years" 
     - Variance of mean values over the growing season 2014-2020. Values between <span style="color:blue">0.0</span> (<span
     style="color:blue">blue</span>) and <span style="color:red">0.52</span>
     (<span style="color:red">red</span>). Small changes in settlement and forest areas are clearly visible.
     - “Hohes Holz” and “Großer Bruch” in the Bode catchment area, Landsat-8, Growing season 2014-2020

To analyze changes over a longer period Imalys provides the ```difference```,
```variance``` and the ```regression``` commands. ```difference``` simply
returns the arithmetical difference between the given images. Corresponding
bands are calculated individually.

Given a stack of at least three images the ```variance``` calculates the
“variability” of each pixel using the Gaussian formulae. ```regression```
quantifies a linear trend in time. Both processes calculate each pixel and each
band individually. If no time stamp is provided by the
[import](../process-definitions/import.md) process, all images will be treated
as of equal distance. Strictly speaking both formulae assume a normalized
distribution of the values.

To reduce the statistical results for different bands to one value for each
pixel, the
[principal](../process-definitions/reduce.md#first-principal-component-of-all-bands)
or the [mean](../process-definitions/reduce.md#arithmetic-mean-bands-or-images)
command can be used.

```bash
IMALYS [tutorial 3b]
home
	directory=»user«.imalys
	clear=true
	log=»tutorial«/results
import
	database=»tutorial«/archives/center.csv
	distance=1.00
	period=20220501-20221031
	frame=»tutorial«/frames/bounding-box.shp
	cover=0.9
	quality=0.86
	bands=_B2, _B3, _B4, _B5, _B6, _B7
	factor=2.75e-5
	offset=-0.2
	warp=32632
	pixel=30
compile
	period=20220501-20221031
reduce
	select=compile
	execute=variance
export
	select=variance
	target=»tutorial«/results/Variance.tif
reduce
	select=variance
	execute=principal
export
	select=principal
	target=»tutorial«/results/Variance-Principal.tif

# »user« must be exchanged with the home directory of the user
# »tutorial« must be exchanged with the path of the tutorial
```

### Workflow 

Tutorial 3b shows how to calculate the variance of the seasonal differences
during one year. To get the differences of the whole vegetation period all
images from May to October 2022 are extracted once again and compiled to one
large stack. If you look at the metadata ```.imalys/compile.hdr``` you can see
how Imalys records sensor, type and date of the different bands.

The
[Regression](../process-definitions/reduce.md#regression-based-on-standard-deviation)
command can be called exactly in the same way. As the ```variance``` will show
the magnitude of the changes, the ```regression``` will indicate the direction.
If longer time periods should be examined, also the typical values of short time
periods like the results of the above tutorials can be used.

## [T3c] Seasonal periods

In mid-latitudes there are pronounced seasonal periods with changes often
greater than the changes over years. On the one hand the seasonal changes
themselves can be of interest (see [T3b](#t3b-time-series-and-change)).

In order to depict typical features, trends or outliers, the season of the
images should be taken into account. For long time series images of the same
season should be preferred. We got no problems to collect enough images for two
40 years time series of Germany, one based on images for May to July, the other
for August to October. The results allow to map long and short time developments
with the same datasets. An example for a 5 years period is depicted below.

![Variance of Seasonal
Differences](../_img/Variance_of_seasonal_differences.png)

!!! info "Variance of Seasonal Differences"
     - **Time periods** May – July and August – October over the years 2017 to 2021
     - Settlements show the smallest seasonal variance, forest (NO) and permanent grassland (S)
     moderate variance. Agriculture show all grades (harvest)
     - **Sensor**: Sentinel-2
     - **Years**: 2017-2021
     - **Bands**: 2, 3, 4, 8
     - **Values**: 0.0 (Blue) – (Red) 0.014

### Features

The extent and direction of periodic changes are a specific feature of many
landscape types. Settlement areas show little periodic change, deciduous forests
show significant but regular fluctuations and arable land both changes, high and
irregular due to harvesting. Permanent grassland can be depicted by its
periodicity.

In the above image water induced periodical changes of the arable land become
visible in the right half of the image.

## [T3d] Outliers

### Outlier

Outliers in time series are often also outliers in space, e.g., construction
sites or forest fires. Outliers in space are easy to detect, especially if
previously visible boundaries ([Zones](../process-definitions/zones.md)) have
been created. If possible, the analysis should always use both aspects.

### Time scale

Optical sensors are not suitable to detect changes over days or weeks. Radar
(e.g. Sentinel-1 in C-Band) provide an image (backscatter) every 2-3 days. Using
radar the date of a rapid change (e.g. harvest) can be determined but the nature
of the change has to be recognized in another way.

Radar backscatter and polarization are very different from optical images.
Smooth, built-up objects usually show a high backscatter. Corner reflectors e.g.
power lines can mimic much larger objects.

Sensors such as MODIS Terra provide optical data on a daily basis, but with at
least 10 times lower spatial resolution than Landsat-8 or Sentinel-2. Combining
temporal high-resolution images with spatial high-resolution images allows
sudden changes to be detected and helps to adjust single recordings to typical
states of annual changes, thus making random recording dates easier to
interpret.

![MODIS-T Vegetation Index](../_img/MODIS-T_Vegetation-Index.png){width=800px}

!!! info "Annual cycle for the vegetation index NIRv"
     - Weekly survey of all green areas in Leipzig over the years 2011-2020. Measurements in the dry
     year 2018 are highlighted in green.
     - MODIS Terra, Public green spaces in Leipzig

## [T3e] Diversity and Contrast

[Kernel](../process-definitions/kernel.md) processes assign a new value to each pixel. The new value is
compiled from a small window (kernel) and assigned to the central pixel of
the kernel. Kernels can be used to determine the local roughness of an image
but also to modify the contrasts or enhance an elevation model.

![Normalized Texture Bode Catchment](../_img/normalized_texture_bode.png){width=800px}

!!! info "Normalized Texture"
     - First principal component of the normalized textures of all optical bands in a 5x5 kernel.
     - **Sensor**: Landsat-8 Values: 0 (blue) to 0.42 (red) Growing season 2014-2020
     - “Hohes Holz” und “Großer Bruch” in the Bode catchment area.

In terms of biology, diversity is defined as the probability to record
different species in one place. Indicators of landscape diversity have been
a focus of our development. Imalys provides different methods to estimate
landscape diversity from image data.

Spectral diversity or texture is traditionally used as a measure of ecological
diversity in remote sensing. Imalys provides additional methods to quantify
spatial distribution, shape and temporal changes of depicted structures.

### Texture

Textures return the “roughness” of an image. In the simplest case, this is the spectral
difference between adjacent pixels in a small window (kernel). The kernel is
systematically dragged over the whole image. Each kernel defines one result
pixel. In addition to the classic texture, the normalized texture (“normal”)
and the Inverse Difference Moment (IDM) according to Haralik⁵ (“inverse”)
are implemented.

### Contrast

Structural features of image data can be strengthened or weakened using kernels.
Imalys implements a [Laplace
transformation](../process-definitions/kernel.md#enhance-the-local-contrast) to
amplify small-scale structures and vice-versa, a
[lowpass](../process-definitions/kernel.md#reduce-the-local-contrast) filter
with a Gaussian kernel to enhance larger structures and reduce noise.

### Rao’s Diversity

Texture as a measure of diversity has the disadvantage that even a
monoculture can show a high texture due to a “rough” surface. {==[Rao’s approach]()==}
evaluates spectral and spatial differences simultaneously. Regular patterns show
lower values than the classical texture. Unfortunately, Rao’s approach is
complicated to calculate.

Imalys implements two versions of Rao’s approach. The pixel-oriented version
([roughness](../process-definitions/kernel.md#raos-diversity-based-on-pixels)) only
differs a little from texture in practice. The class-oriented version
([entropy](../process-definitions/kernel.md#raos-diversity-based-on-classes)) is
closer to the biological definition, but requires a land cover classification.

```bash
IMALYS [tutorial 3e]
home
	directory=»user«.imalys
	clear=false
	log=»tutorial«/results
compile
	select=»tutorial«/results/B234567_20220501-20220731.tif
	select=»tutorial«/results/B234567_20220801-20221031.tif
kernel
	select=compile
	execute=normal
	execute=deviation
	execute=laplace
	radius=2
export
	select=normal
	target=/home/c7sepe2/tutorial/results/Normal_20220501-20221031.tif
export
	select=deviation
	target=/home/c7sepe2/tutorial/results/Deviation_20220501-20221031.tif
export
	select=laplace
	target=/home/c7sepe2/tutorial/results/Laplace_20220501-20221031.tif

# »user« must be exchanged with the home directory of the user
# »tutorial« must be exchanged with the path of the tutorial
# ! The result of the “compile” command of  tutorial 3b should exist.
```

### Workflow

Tutorial [T3e] shows how to calculate three different [Kernel
processes](../process-definitions/kernel.md). All of them use the same input and
store their result with the name of the process at the working directory. The
“normal” contrast kernel returns the relative brightness differences of
neighboring pixels. The results are similar to the statistically based
“deviation” but differ if the landscape shows a regular pattern. ```deviation```
returns almost identical results as Rao’s ß-Diversity
([roughness](../process-definitions/kernel.md#raos-diversity-based-on-pixels))
but is much easier to calculate. The [laplace
transformation](../process-definitions/kernel.md#enhance-the-local-contrast)
uses a “Mexican head” function to enhance contrast and visualize borders. All
[Kernel processes](../process-definitions/kernel.md) use the first principal
component of all input bands to calculate their result. The
[export](../process-definitions/export.md) command stores the result as Geo-Tiff
at the “results” directory of the tutorial.

## [T3f] Rao’s Entropy

Rao’s Entropy calculates a diversity measure based on the local image contrast.
Using single pixels the calculation is quite demanding. If a spectral
classification is available, the same process is much quicker. The
[mapping](../process-definitions/mapping.md) command with ```entropy``` as a
parameter calculates both, the classification and Rao’s Entropy.

The main concept is to compare each pixel with each other in a given kernel.
Thus regular pattern with high texture like a forest canopy will show low to
moderate diversity. Random distribution will show the highest values
([entropy](../process-definitions/kernel.md#raos-diversity-based-on-classes)).

The classification needs no references because Rao’s entropy only needs the
frequency of the different classes within one kernel and the spectral
differences between the them. Rao’s Entropy can only be retrieved under
[mapping](../process-definitions/mapping.md), since classification and diversity
have to be computed together. The classification command
[mapping](../process-definitions/mapping.md) needs additional parameters but the
defaults will work in almost any situation.


```bash
IMALYS [Tutorial 3f “entropy”]
home
	directory=»user«.imalys
	clear=false
	log=»tutorial«/results
compile
	select=»tutorial«/results/B234567_20220501-20220731.tif
	select=»tutorial«/results/B234567_20220801-20221031.tif
mapping
	select=compile
	model=pixel
	features=30
	samples=30000
	execute=entropy
	radius=2
export
	select = entropy
	target = »tutorial«/results/Entropy.tif

# »user« must be exchanged with the home directory of the user
# »tutorial« must be exchanged with the path of the tutorial
# ! The result of the “compile” command of tutorial 3b should exist
```

### Workflow

Tutorial 3f shows how to use the self adjusting classification
[mapping](../process-definitions/mapping.md) as input to calculate Rao’s
Entropy. [mapping](../process-definitions/mapping.md) creates a spectral
classification with 30 different clusters based on 30,000 samples. The
```model```, ```features``` and ```sample``` parameters are default and can be
modified if necessary. The [entropy
process](../process-definitions/kernel.md#raos-diversity-based-on-classes)
depends on the classification and can only be induced under
[mapping](../process-definitions/mapping.md). The classification result and the
class definition is stored as “mapping” in the working directory and can be used
for other processes.

## [T3g] Structural features

Landscape diversity can also be calculated for
[zones](../process-definitions/zones.md). If “diversity” is added to the
[feature](../process-definitions/features.md) list, the command will add a
diversity attribute to each zone. Since zones combine small-scale regions, this
process resembles Rao’s entropy.
