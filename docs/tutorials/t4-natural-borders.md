# [T4] Natural Borders (Zones)

Imalys uses a process that divides images into zones with widely homogeneous
spectral characteristics ([Index](../process-definitions/index.md)). The process
only depends on the image data and a parameter for the mean size of the zones.
Zones have a geometry, attributes and individual neighbors. The borders between
zones represent borders of land use. The local density of landuse borders is
used as an ecological indicator (**effective mesh size** or **coherence degree**
(Jaeger 2001[^1])).

![Structural landscape elements](../_img/structural_landscape_elements.png){width=800px}

!!! info "Structural Landscape Elements"
     - Zones with different combinations of optical characteristics are marked by dark narrow lines
     Once created, zones can be processed separately from the image data.
     - “**Hohes Holz**” and “**Großer Bruch**” in the Bode catchment area, **Landsat-8** images, first half of the growing season **2014-2020**.

With pattern analysis of the image, areas with mostly the same optical
characteristics can be defined ({==[Index](../process-definitions/index.md)==}). They
are hereinafter referred to as **zones**. This technique is also known as Object
Based Image Analysis (OBIA) (Blaschke 2014 [^2]).

## [T4a] Border delineation

The process is based on an iterated watershed algorithm. Details are explained
in {==cape. “Methods”==}. The algorithm can process any type of image, regardless of
the image source (microwave, altitude data, light), the scale or the number of
bands. The algorithm sets borders with a preference for those places with
maximum contrast.

### Size, Borders

The average size of the zones can be freely selected. The process starts with
**zones** of individual pixels and gradually removes borders between existing
zones until a threshold is reached. The order in which the borders are removed
does not depend on the final size. This means that larger zones can act as a
second-degree order for smaller ones. Larger zones inherit the borders of the
smaller ones.

### Input data

If bands with very different value ranges are used, the bands with the largest
numerical values dominate the position of the border. It may be useful to scale
the values of the different bands before processing
({==[Equalize](../process-definitions/index.md)==}) so that all features will
have the same influence on the result.

### Definition

The internal representation consists of an image with the zone ID as a value
(index, "index.hdr"), a WKT file with the coordinates of the borders
(```vector.csv```), an attribute table ("index.bit") and a table of all the
links between adjacent zones ("topology.bit").

### Features

The process assigns all spectral bands of the given image as attributes of the
delineated zones. New [Features](../process-definitions/features.md) can be
added or deleted as necessary.

```bash
IMALYS [tutorial 4a]
home
    directory=»user«.imalys
    clear=false
    log=»tutorial«/results
compile
    select=»tutorial«/results/B234567_20220501-20220731.tif
    select=»tutorial«/results/B234567_20220801-20221031.tif
zones
    select=compile
    bonds=low
    size=70
export
    select = index
    target = »tutorial«/results/Zones.shp

# »user« must be exchanged with the home directory of the user
# »tutorial« must be exchanged with the path of the tutorial
# the result of the “compile” command of tutorial 3b should exist
```

Totorial 4a shows how to create zones and export them as polygons. In contrast
to the previous examples the image data are taken from an earlier result.
[Compile](../process-definitions/compile.md) will bring the image back to the
working directory.

The [zones](../process-definitions/zones.md) process creates a raster mask
**index** at the working directory together with an attribute table
```index.bin``` and a size and contact database of all zones called
```topology.bit```. The [export](../process-definitions/export.md) process
delineates the borders of the different zones and stores them as a geometry file
with attributes. The ending of the **target** filename controls the file format.

Size and shape of the zones should fit your intentions. ```limit=...``` will
restrict the variance of single zones to the given value. ```size=...``` will
restrict the mean size of the zones. In this case the **limit** is set to an
extreme high value so only the size will have an effect. No entry combination
will fit all purposes. It is convenient to test different entries of the
parameters **limit** and **size**. To simplify the comparison a ```zones.shp```
file is created at the working directory even if no
[export](../process-definitions/export.md) is induced.

Zones can be created as cascades where smaller zones fill larger ones without
interference (see [zones](../process-definitions/zones.md)).

## [T4b] Spectral and structural features

Spectral features are calculated as the mean of all pixels within one zone.
Structural features can be derived from the geometry and the connection of the
zones. Some features describe individual zones such as **size** (area) or
**dendrites** (shape), others describe the connections to adjacent zones such as
**relation** (density of neighbors) or **diversity** (spectral differences). New
[features](../process-definitions/features.md) can rely on images that where not
included in the delineation of the zones.

![Dendritic Cell Form](../_img/dendritic_cell_form.png){width=800px}

!!! info "Dendritic Cell Form"
     - Ratio of volume and area of single zones.
     - Small or narrow zones have small values, large or compact zones have large values. The quotient does not depend on the parameters of the cell formation.
     - **Process**: Dendrites
     - **Values**: 0.05 – 1.0 (blue – red)
     - **Location**: Bode catchment area
     - **Sensor**: Sentinel-2
     - **Years**: 2017-2021, May – Oct.

### Shape and size

[Features](../process-definitions/features.md) adds or extends an attribute
table to the zones geometry. The shape and connections of the zones are obtained
directly from the geometry of the zones. In addition, arbitrary image data such
as height or [kernel](../process-definitions/kernel.md) results can be linked to
the zones as new attributes. [Features](../process-definitions/features.md) was
therefore implemented as an independent process. The command can be used as
often as you like with different parameters.

### Size

The size of the zones ([zones ->
size](../process-definitions/zones.md#select-the-mean-size-of-the-zones))
depends on a selectable parameter and thus it cannot be used to compare images.
Therefore all other structural attributes return relative values that are less
dependent of the absolute size of the zones.

### Connections

Two other processes compare the shape and size of the zones with the local
environment.
[Proportion](../process-definitions/features.md#size-diversity-of-the-central-zone-and-all-neighbors)
gives the ratio of the central zone to all connected zones. The value is greater
than one if the central zone is larger than the mean of the neighbors.
[Relation](../process-definitions/features.md#quotient-of-number-of-neighbors-and-cell-perimeter)
gives the ratio between the perimeter and the number of connected zones. As with
[Dendrites](../process-definitions/features.md#quotient-of-zone-perimeter-and-zone-size),
size and environment include the value of →Relation.

### Local concentration

The shape and size of zones are not usually randomly distributed, they form
regions or corridors with similar characteristics.
[Diffusion](../process-definitions/features.md#smooth-features-in-a-network-of-zones)
strengthens locally dominant features in a similar way to a low pass filter and
thus makes focal points more visible. The algorithm follows {==Alan Turing’s
suggestion==} to understand patterns as a consequence of diffusing fluids (ZZZ).

![Local concentration for the Dendrites attribute](../_img/local_concentration_dendrites.png){width=800px}

!!! info "Local concentration for the Dendrites attribute"
     - Regional balance of values over 5 levels enhances the visibility of focal points and corridors.- **Process**: Dendrites
     - **Process**: Dissemination
     - **Years**: 2014 – 2020
     - **Values**: 0.09 – 5.6 (red – blue)
     - **Sensor**: Landsat-8
     - **Site**: Bode catchment area

If corridors, i.e. paths with favorable conditions for exchange, are to be
found, a hydrological drainage model {==[runoff](../process-definitions/index.md)==} is available, which does not only
have to use elevation data. A classification ({==[pixel](), [zonal]())==} or image objects
([fabric](../process-definitions/fabric.md)) can also be used to record large scale spatial links.

```bash
IMALYS [tutorial 4b “attributes”]
home
    directory=»user«.imalys
    clear=false
    log=»tutorial«/results
compile
    select=»tutorial«/results/B234567_20220501-20220731.tif
    select=»tutorial«/results/B234567_20220801-20221031.tif
    select=»tutorial«/results/NirV_20220501-20220731.tif
    select=»tutorial«/results/NirV_20220801-20221031.tif
    select=»tutorial«/results/Deviation_20220501-20221031.tif
    select=»tutorial«/results/Normal_20220501-20221031.tif
    select=»tutorial«/results/Laplace_20220501-20221031.tif
features
    select=compile
    execute=cellsize
    execute=dendrites
    execute=proportion
    execute=diversity
    execute=relation
export
    select = index

# target = »tutorial«/results/Zones_20220501-20221031.shp
# »user« must be exchanged with the home directory of the user
# »tutorial« must be exchanged with the path of the tutorial
# the results of some of the earlier tutorials must exist

```

Tutorial 4b shows how to create an attribute table from images and the geometry
of the zones. The images must exist. If more than one image should be
transferred, all images must be stacked using the
[compile](../process-definitions/compile.md) command and selected as one image.
Shape, size and connection of the zones only relay on the geometry of the zones.
They are created by the given parameters like **dendrites** or **proportion**.

The parameter **delete** completely resets the attribute table. Without
**delete** the new attributes are added to the existing ones. Some features like
**diversity** relay on both, spectral and geometrical features. Without spectral
input they will not work (see [features](../process-definitions/features.md)).

The [export](../process-definitions/export.md) command links geometry and
attribute table of the zones. To work with the export is mandatory.

[^1]: {==Jaeger 2001==}
[^2]: {==Blaschke 2014==}
