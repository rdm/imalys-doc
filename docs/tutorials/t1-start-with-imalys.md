# [T1] Start with Imalys

## Initialize Imalys processes

Imalys is controlled by a text file (hook) containing commands and parameters.
To simplify long process chains, all commands and parameters are given in one
text file or hook. The first entry is always the
[Home](../process-definitions/home.md) command with information where to store
intermediate data and where to store the metadata.

```bash
IMALYS [tutorial 1b] 
home 
    directory = »user«/.imalys 
    clear = true 
    log = »tutorial«/results

# »user« must be exchanged by the users home directory
# »tutorial« must be exchanged by the directory of the tutorial
```

The [Home](../process-definitions/home.md) command and ```IMALYS``` at the
beginning of the first line is mandatory for each process chain. ```»user«```
stands for the home directory of the user. The default working directory
“»user«/.imalys” is used and the directory is cleared at the beginning of the
process chain. The directory ```»user«/results``` - Setup: setup/index.mdis assigned or created to
store messages and metadata. Each process and many sub-steps return messages
about the progress of the program. We recommend to store these files together
with the results.

## General Syntax of the Process Chain

### Process chain

The process chain passed to ```xImalys``` must contain commands and their
parameters. Imalys processes can be combined as necessary. The internal logic of
the process chain is up to the user. The [home](../process-definitions/home.md)
command to create or assign a working directory at the beginning of the process
chain is mandatory.

Each command and each parameter needs a separate line. A single word in one line
is interpreted as process name. Lines with a ```=``` character are interpreted
as ```parameter = value``` pair. Parameters always have a preset. It is changed
by the entry in the text. Only the ```select``` parameter to assign an
appropriate input for each process is mandatory.

### Comments

Everything after the ```#``` sign will be ignored in the same line.

### Working directory

The working directory was implemented to enable the rapid processing of data
that may only be available through a service or a slow connection. It should be
directly accessible. The process chain is bound to the working directory. It can
be created or emptied at the beginning of the chain and stores all  intermediate
results. Each instance of xImalys needs its own working directory.

### Result names

Besides the [export](../process-definitions/export.md) command each process
stores the results to the working directory. The result file name is the same as
the process name. If the results are transferred to tables, the process name
also serves as field name. Therefore process names are short and can have a much
wider meaning in general usage.

The default names can be changed using the →Target command. Existing files will
be overwritten without warning. All images are stored as raw binary with ENVI
header. Geometries use the WKT-format. Imalys thus complies with the
{==[requirements]()==} of the [European Space Agency (ESA)](https://www.esa.int).
