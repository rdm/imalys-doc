# [T5] Objects and Classification

Imalys provides a fully automatic classification of image features
[mapping](../process-definitions/mapping.md). The process can use pixels or
zones and in an advanced version zones can be combined to **objects**.

![Self-Adjusting Classification](../_img/self_adjusting_classification.png){width=800px}

!!! info "Self-Adjusting Classification"
     - 30 spectral combinations (patterns) separated by a fully automatic process according to Teuvo Kohonen
     - The colors shown are random.
     - **Sensor**: Sentinel-2
     - **Years**: 2017-2021
     - **Bands**: 2, 3, 4, 8
     - **Seasons**: May – July and August – October
     - **Process**: Mapping
     - **Color code**: Random
     - **Values**: Classes 1...30

Deriving basic land use types from image data can be unreliable because land use
types are defined by their purpose and not by their appearance in the image.
Machine learning can recognize almost any pattern, but needs to be trained using
examples. Only trained patterns can be recognized. The training might even take
longer than manual classification.

## Levels

Imalys implements methods to arrange image features of all kinds into separate
groups or clusters ([mapping](../process-definitions/mapping.md)). The result
reflects feature combinations that are common in the classified image.
[Mapping](../process-definitions/mapping.md) can classify images at three
levels:

1. Pixels based on their spectral combinations
2. Zones based on the zones attributes
3. Objects based on the connections of classified zones

The principle is the same in each case. Features or properties create a
multidimensional feature space. Local concentrations of feature combinations are
detected and classified using a neural network as suggested by Teuvo Kohonen
[^1]

## [T5a] Pixel based classification

### Pixels

The unsupervised classification of pixels based on spectral combinations is a
standard procedure
([pixel](../process-definitions/mapping.md#classifies-spectral-combinations)).
The result mainly depends on the selection and quality of the image base (see
section on Time Series).

```bash
IMALYS [tutorial 5a “automap”]
home
    directory=»user«.imalys
    clear=true
    log=»tutorial«/results
compile
    select = »tutorial«/results/B234567_20220501-20220731.tif
    select = »tutorial«/results/B234567_20220801-20221031.tif
mapping
    select=compile
    model=pixel
    features=14
    samples=30000
export
    select = mapping
    target = »tutorial«/results/Automap.tif
```

Tutorial 5a shows how to classify a multispectral and multitemporal image for 30
spectral clusters. The images at the ```results``` folder must exist. The ```model```
parameter selects a pixel based classification, 30 different clusters are
selected by ```features```. The ```samples``` are used to train the classifier. The
number of the ```samples``` should be about 1000 times larger than ```features```.
If rare features should be classified, a higher number might be given.

[Mapping](../process-definitions/mapping.md) stores the result image and the
class definition to the working directory.
[Export](../process-definitions/export.md) saves both as ```automap.tif``` to
the **results** directory of the user.

## [T5b] Zone based classification

[Zones](../process-definitions/zones.md) must be created before the
classification but zones properties can provide additional features derived from
the size, shape and connections of the zones. The spectral features of zones are
the mean of all pixels that contribute to the zone. Zones and classes complement
each other. Zones summarize typical pixel features for a limited area.
Classification sorts them into a manageable list of feature combinations.
Typical problems with clustering pixels such as **pepper and salt** patterns do
not occur anymore.

![Image Objects I](../_img/image_objects_i.png){width=800px}

!!! info "Image Objects I"
     - Spatial patterns of different zones were reduced by the self-calibrating process to 30 patterns, which were assigned to 16 classes.
     - Process and parameters are identical to [Image Objects II](#t5c-object-based-classification)
     - **Image data**: Sentinel-2
     - **Date**: 16.9.2018
     - **Bands**: 8-4-3-2
     - **Process**: [fabric](../process-definitions/fabric.md)
     - **Values**: Classes (see legend)

```bash
IMALYS [tutorial 5b “mapping”]
home
    directory=»user«.imalys
    clear=true
    log=»tutorial«/results
compile
    select = »tutorial«/results/B234567_20220501-20220731.tif
    select = »tutorial«/results/B234567_20220801-20221031.tif
zones
    select = compile
    limit = 10000
    size = 100
mapping
    select=index
    model=zones
    features=14
    samples=30000
export
    select = index
    target = »tutorial«/results/Zones.shp
export
    select = mapping
    target = »tutorial«/results/Mapping.tif
```

Tutorial 5b shows how to classify zones instead of pixels. The image input is
taken from an earlier result. Alternatively all images stored at the working
directory can be selected directly by [zones](../process-definitions/zones.md).
To classify zones with the [mapping](../process-definitions/mapping.md) process,
the raster part (```index``` and ```index.hdr```) of the zones definition must
be called. The classification result ```mapping``` can be exported as usual.
Different classes are visualized by a color palette.

## [T5c] Object based classification

Classified zones can be combined by a second level classification to form
**objects** ([fabric](../process-definitions/fabric.md)). Objects are only
defined by the frequency or intensity of the connections between their zones.
The connection intensities form characteristic patterns that can be classified.
The result describes a typical pattern of different spectral and spatial
combinations. These classes are hereinafter referred to as **objects**.

![Image Objects II](../_img/image_objects_ii.png){width=800px}

!!! info "Image Objects II"
     - Spatial patterns from different zones were reduced by a self-calibrating analysis to 30 patterns, which were assigned to 13 different classes.
    - The process and parameters are identical to [Image Objects I](#t5b-zone-based-classification)
     Image data: Infrared aerial photos and elevation model of the State of Saxony.

### Object Size

The size of the objects is not limited. Simple patterns of small zones can be
repeated over a large area to form one large object. Large zones usually form
objects of one dominant zone class and many smaller ligands. In practice the
size of the zones should be selected in such a way that homogeneous objects are
not reduced to many zones, whereas heterogeneous objects can be represented by a
sufficient number of smaller zones.

```bash
IMALYS [objects]
home
    directory=»user«.imalys
    clear=true
    log=»tutorial«/results
compile
    select = »tutorial«/results/B234567_20220501-20220731.tif
    select = »tutorial«/results/B234567_20220801-20221031.tif
zones
    select = compile
    limit = 10000
    size = 100
mapping
    select=index
    model=fabric
    features=14
    samples=30000
export
    select = mapping
    target = »user«/results/Objects.tif
export
    select = index
    target = »user«/results/Zones.shp
```

The call of an object based classification is almost identical to a zones based
one even if the internal processes differ considerably. The only real difference
is the ```double``` option that will extend the search for connected zones.

## [T5d] Extended object features

### Seasons

The spectral differentiation of natural areas becomes more reliable when images
from different seasons are used together. Different seasons may originate from
different years. A logical combination of very different parameters, including
the color, shape, distribution and development of surface characteristics, may
provide a robust alternative to machine learning without extensive training [^2].

### Examples

A purely statistical object classification without any training
([fabric](../process-definitions/fabric.md)) was sufficient to separate roof
surfaces, streets, parks, small gardens and other elements from two very
different images of the city of Leipzig. [Image Object
I](#t5b-zone-based-classification) shows the result for an satellite image,
[Image Object II](#t5c-object-based-classification) the result for an areal
image combined with an elevation model. In both cases identical process
parameters were selected.

### Transfer

Due to this statistical method it only generates definitions for objects that
are common in the image. Other landscapes or a different light invalidate the
definitions. The technology has the advantage that it does not require any
training or prior information. The result is only determined by the image data.
It has the disadvantage that the results can only be transferred into images
with the same structure. The technical implementation is described in detail
in the [background](../background/index.md) section.

## [T5e] Comparison, Class Reference

It is not easy to determine a temporal, spatial or combined spatio-temporal
linking of images and other measurements. ESIS offers the process
[rank](../process-definitions/rank.md), which allows any measurement series to
be compared using a rank correlation. For this purpose, the time of the data
acquisition or the location (preferably both) must be known in both datasets.

### References

[Compare](../process-definitions/compare.md) returns two cross reference tables
between the most recent classification result an a reference given as raster
image or as polygons.

### Correlation

[Rank](../process-definitions/rank.md) works with each distribution and can
therefore analyze any pattern. As an alternative, the degree of correlation can
be estimated according to X²→ChiSquare. Strictly speaking, X² presupposes
standard distribution, but can then be more selective.

[^1]: {==Kohonen1982==} 
[^2]: {==Banzhaf2018==}