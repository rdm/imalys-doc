# [T2] Seamless image products

Satellite image data are shipped in compressed archives, the tracks are cut into
tiles, different bands are stored to different files and the image values
support compression. The [import](../process-definitions/import.md) and the
[compile](../process-definitions/compile.md) command are designed to
collect the necessary data and combine them to a seamless and calibrated image
within a given frame. The second task of the [import](../process-definitions/import.md)
process is to store all images with a common projection, pixel size and file format.

![IMAGE](../_img/Selection%20points%20and%20image%20data%20for%20entomological%20research%20north%20of%20Madrid.png){width=800px}

!!! info "Selection points and image data for entomological research north of Madrid"
	 - **Sensor**: Sentinel-2
	 - **Bands**: 8-4-3 (Infrared)
	 - **Date**:12 June 2021
	 - **Calibration**: TOA Reflectance
	 - **Section**: Test area
	 - **Source**: ESA

The [import](../process-definitions/import.md) command uses a lot of parameters
to return most appropriate images. ```select``` accepts the archive name,
```frame``` cuts the scene to a given region, ```period``` selects the
acquisition time, ```quality``` rejects images with more than the given
partition of errors, ```warp``` and ```pixel``` change the projection and
finally ```factor``` and ```offset``` transfer the result into calibrated
values. Except of ```select```, none of these parameters are mandatory.

## [T2a] Extract, calibrate and crop images

The first tutorial will import the 6 optical bands of a known Landsat image, cut
them to the frame to a ROI (Region Of Interest) and calibrate the result to TOA
(Top Of Atmosphere) reflectance.

### Select

If the image source is known, the archive name(s) can be given with
```select```. ```select``` can be repeated as often as necessary. The archives
must exist.

### Frame

```select``` without ```frame``` will import the whole tile. The ```frame```
process cuts out the ROI. Imalys expects a vector file to define the ROI. The
result will be a rectangle including all points of the given geometry.

```bands```

By default [import](../process-definitions/import.md) will process each image of
the archive. This will be rarely necessary and the calibration can depend on the
image type. Optical and thermal bands are an example for this point.

### Calibration

To compare images or calculate indices the values of the archived images must be
calibrated. TOA reflectance is the most common format, radiation [W/m²] is
another option. The calibration parameters are part of the image metadata. As an
example, ```factor``` and ```offset``` for TOA reflectance are given for the
optical bands of Landsat 5-9 and Sentinel 2:

| Satellite | Factor | Offset | 
| ---- | ---- | ---- | 
| Landsat 5,7,8 | 2.75e-4 |	-0.2 |
| Sentinel-2 | 1e-4 | 0 |

```bash
IMALYS [tutorial 2a]
home
	directory=»user«/.imalys
	clear=true
	log=»tutorial«/results
import
	select=»tutorial«/archives/LC08_L2SP_193026_20220515.tar
	select=»tutorial«/archives/LC08_L2SP_193026_20220531.tar
	select=»tutorial«/archives/LC08_L2SP_193026_20220702.tar
	select=»tutorial«/archives/LC08_L2SP_193026_20220718.tar
	frame=»tutorial«/frames/bounding-box.shp
	quality=0.86
	bands=_B2, _B3, _B4, _B5, _B6, _B7
	factor=2.75e-5
	offset=-0.2
	warp=32632
	pixel=30
# »user« must be exchanged with the home directory of the user
# »tutorial«  must be exchanged with the path of the tutorial
```

### Workflow

The [home](../process-definitions/home.md) process is necessary for each process
chain (see [initialize Imalys processes](t1-start-with-imalys.md#initialize-imalys-processes)).

The [import](../process-definitions/import.md) process “selects” four Landsat-8
OLI images, extracts them from the tar-archive, cuts them to the extend of the
“bounding-box”, rejects images with less than 85% clear pixels, selects the six
optical bands of the OLI sensor, calibrates the values to TOA reflectance,
transforms the projection to WGS-84, UTM, zone 32 with 30 m pixel size and
stacks the 6 optical bands to one image.

The intermediate results are stored at the working directory
```»user«/.imalys```. The extracted images are named according to the sensor,
the tile and the date of the image acquisition.

## [T2b] Enhance image quality

A main problem of satellite images are dropouts due to cloud coverage. Less
noticeable but of similar importance is the landscape change in time. Each image
is a snapshot. Dropouts and changes can be controlled by collecting information
of different but similar images. Tutorial 2b will show how to extract a seamless
image with typical values out of a couple of patchy images.

### Median

[Reduce](../process-definitions/reduce.md) with ```execute=bestof``` uses the
median to return the most common value for each pixel in an image stack. This
can be used to return a “typical” value for a time period of a few weeks or
months. Almost everywhere clouds are random in time. If the majority of single
pixels are clean, the most common value is free of clouds and cloud shadows. To
get an impression how the process works, compare the imported images from
October, September and August ([tutorial 2d](#t2d-import-with-an-image-catalog),
working directory) with the result of the “bestof” process.

```bash
IMALYS [tutorial 2b]
home
	directory=»user«/.imalys
	log=»tutorial«/results
compile
	period=20220501-20220731
reduce# [T2] Seamless image products
```

Satellite image data are shipped in compressed archives, the tracks are cut into
tiles, different bands are stored to different files and the image values
support compression. The [import](../process-definitions/import.md) and the
[compile](../process-definitions/compile.md) command are designed to
collect the necessary data and combine them to a seamless and calibrated image
within a given frame. The second task of the [import](../process-definitions/import.md)
process is to store all images with a common projection, pixel size and file format.

![IMAGE](../_img/Selection%20points%20and%20image%20data%20for%20entomological%20research%20north%20of%20Madrid.png){width=800px}

!!! info "Selection points and image data for entomological research north of Madrid"
	- **Sensor**: Sentinel-2
	- **Bands**: 8-4-3 (Infrared)
	- **Date**: 12 June 2021
	- **Calibration**: TOA Reflectance
	- **Section**: Test area
	- **Source**: ESA

The [import](../process-definitions/import.md) command uses a lot of parameters
to return most appropriate images. ```select``` accepts the archive name,
```frame``` cuts the scene to a given region, ```period``` selects the
acquisition time, ```quality``` rejects images with more than the given
partition of errors, ```warp``` and ```pixel``` change the projection and
finally ```factor``` and ```offset``` transfer the result into calibrated
values. Except of ```select```, none of these parameters are mandatory.

## [T2a] Extract, calibrate and crop images

The first tutorial will import the 6 optical bands of a known Landsat image, cut
them to the frame to a ROI (Region Of Interest) and calibrate the result to TOA
(Top Of Atmosphere) reflectance.

### Select

If the image source is known, the archive name(s) can be given with
```select```. ```select``` can be repeated as often as necessary. The archives
must exist.

### Frame

```select``` without ```frame``` will import the whole tile. The ```frame```
process cuts out the ROI. Imalys expects a vector file to define the ROI. The
result will be a rectangle including all points of the given geometry.

```Bands``` 

By default [import](../process-definitions/import.md) will process each image of
the archive. This will be rarely necessary and the calibration can depend on the
image type. Optical and thermal bands are an example for this point.

### Calibration

To compare images or calculate indices the values of the archived images must be
calibrated. TOA reflectance is the most common format, radiation [W/m²] is
another option. The calibration parameters are part of the image metadata. As an
example, ```factor``` and ```offset``` for TOA reflectance are given for the
optical bands of Landsat 5-9 and Sentinel 2:

| Satellite | Factor | Offset |
| ---- | ---- | ---- |
| Landsat 5,7,8 | 2.75e-4 |	-0.2 |
| Sentinel-2 | 1e-4 | 0 |

```bash
IMALYS [tutorial 2a]
home
	directory=»user«/.imalys
	clear=true
	log=»tutorial«/results
import
	select=»tutorial«/archives/LC08_L2SP_193026_20220515.tar
	select=»tutorial«/archives/LC08_L2SP_193026_20220531.tar
	select=»tutorial«/archives/LC08_L2SP_193026_20220702.tar
	select=»tutorial«/archives/LC08_L2SP_193026_20220718.tar
	frame=»tutorial«/frames/bounding-box.shp
	quality=0.86
	bands=_B2, _B3, _B4, _B5, _B6, _B7
	factor=2.75e-5
	offset=-0.2
	warp=32632
	pixel=30
# »user« must be exchanged with the home directory of the user
# »tutorial«  must be exchanged with the path of the tutorial
```

### Workflow

The [home](../process-definitions/home.md) process is necessary for each process
chain (see [initialize Imalys processes](t1-start-with-imalys.md#initialize-imalys-processes)).

The [import](../process-definitions/import.md) process “selects” four Landsat-8
OLI images, extracts them from the tar-archive, cuts them to the extend of the
“bounding-box”, rejects images with less than 85% clear pixels, selects the six
optical bands of the OLI sensor, calibrates the values to TOA reflectance,
transforms the projection to WGS-84, UTM, zone 32 with 30 m pixel size and
stacks the 6 optical bands to one image.

The intermediate results are stored at the working directory
```»user«/.imalys```. The extracted images are named according to the sensor,
the tile and the date of the image acquisition.

## [T2b] Enhance image quality

A main problem of satellite images are dropouts due to cloud coverage. Less
noticeable but of similar importance is the landscape change in time. Each image
is a snapshot. Dropouts and changes can be controlled by collecting information
of different but similar images. Tutorial 2b will show how to extract a seamless
image with typical values out of a couple of patchy images.

### Median

[Reduce](../process-definitions/reduce.md) with ```execute=bestof``` uses the
median to return the most common value for each pixel in an image stack. This
can be used to return a “typical” value for a time period of a few weeks or
months. Almost everywhere clouds are random in time. If the majority of single
pixels are clean, the most common value is free of clouds and cloud shadows. To
get an impression how the process works, compare the imported images from
October, September and August ([tutorial 2d](), working directory) with the
result of the “bestof” process.

```bash
MALYS [tutorial 2b]
home
	directory=»user«/.imalys
	log=»tutorial«/results
compile
	period=20220501-20220731
reduce
	select=compile
	execute=bestof	
export
	select=bestof
	target=»tutorial«/results/B234567_20220501-20220731.tif

# »user« must be exchanged with the home directory of the user
# »tutorial«  must be exchanged with the path of the tutorial
```

### Workflow

Tutorial 2b uses the results of [tutorial
2a](t2-seamless-image-products.md#t2a-extract-calibrate-and-crop-images). The
default working directory ```»user«/.imalys``` is assigned by
[home](../process-definitions/home.md) but not cleared to use the imported
images for subsequent processes.

The [compile](../process-definitions/compile.md) stacks all selected images,
controls the position of different frames and records the image quality for the
different layers. The result name includes sensor and time. The ```period```
process stacks all bands taken from May to July.
[compile](../process-definitions/compile.md) is mandatory if different images
should be compared or processed together.

The [reduce](../process-definitions/reduce.md) process transforms the compiled
stack to a new image. ```bestof``` reduces a multi image stack to one image with
the same bands or colors as one of the original images. The ```bestof``` process
tries to return the most significant content of several bands (see
[reduce](../process-definitions/reduce.md)). ```bestof``` depends on the
extended image metadata that [compile](../process-definitions/compile.md)
provides.

The [export](../process-definitions/export.md) process transfers the image
format to Geo-TIFF and stores the result to a freely selected place. The new
image format is defined by the extension. No extension will select the ENVI
labeled format as at the working directory.

The results of the [compile](../process-definitions/compile.md) and the
[reduce](../process-definitions/reduce.md) process are called without a path
name. If no path is given, Imalys looks at the working directory.

## [T2c] Archive Catalog (Image collection database)

Imalys can create a local image archives database to select images effectively.
The database stores the acquisition date together with the center and the size
of the archived image tiles. The database is formatted as point geometry to have
a look at it using a GIS.

There is no need to call the [catalog](../process-definitions/catalog.md) in the
same process chain as the [import](../process-definitions/import.md). The
catalog must only be recalculated if the collection of archives has changed.

```bash
IMALYS [tutorial 2c]
home
	directory=»user«/.imalys
	clear=true
	log=»tutorial«/results
catalog
	archives=»tutorial«/archives/*
	target=»tutorial«/archives/center

# »user« must be exchanged with the home directory of the user
# »tutorial«  must be exchanged with the path of the tutorial
```

### Workflow

Tutorial 2c creates a position and size database of all archives at the
directory “archives” and stores it as ```center.csv```. The accepted archives
can be filtered using a usual file name filter like *L2SP* for Landsat level-2
products.

## [T2d] Import with an Image Catalog

The image catalog was designed to select appropriate image tiles out of a large
collection of archives.

### Tile selection

If a ```frame``` is given, only tiles that cover or touch the frame will be
selected. The ```distance``` parameter controls the relative distance between
the center points of the image archives and the given frame. With ```distance =
1``` either the image must cover at least half of the frame or at least half of
the image must cover a large frame. Smaller inputs will force higher coverages.
The parameter ```period``` defines a schedule for the acquisition time.

### Coverage

Sometimes large parts of the image tiles are empty. The parameter ```cover```
checks the coverage of the given frame by the defined parts of the image. The
```quality``` process will check values and status of each pixel within the
frame. This check needs considerable processing time. The preselection of
appropriate image tiles speeds up the import for several times.

### Quality

After the selection of appropriate archives Imalys checks the image quality of
the selected frame. ```quality``` checks if parts of the defined image shows
clouds or other disturbances and limits the proportion of “bad” pixels within
the given frame.

```bash
IMALYS [tutorial 2d]
home
	directory=»user«/.imalys
	clear=true
	log=»tutorial«/results
import
	database=»tutorial«/archives/center.csv
	distance=1.00
	period=20220501-20221031
	frame=»tutorial«/frames/bounding-box.shp
	cover=0.9
	quality=0.86
	bands=_B2, _B3, _B4, _B5, _B6, _B7
	factor=2.75e-5
	offset=-0.2
	warp=32632
	pixel=30

# »user« must be exchanged with the home directory of the user
# »tutorial«  must be exchanged with the path of the tutorial
```

### Workflow 

Tutorial 2d uses an image [catalog](../process-definitions/catalog.md) to import
all example images.

[Home](../process-definitions/home.md) clears the working directory, the archive
selection is set to a acquisition date between May and October and an assured
coverage (distance=1). The result images must be covered at least by 90% of each
accepted image (cover=0.9), the cloud cover must be less than 15%
(quality=0.86).

Everything else is like
[tutorial 2b](t2-seamless-image-products.md#t2b-enhance-image-quality).

## [T2e] Select and reduce a short time course

Time periods can be selected during the image
[import](../process-definitions/import.md) but as well with the
[compile](../process-definitions/compile.md) command if imported images are
available. The selection will only work if the acquisition time is part of the
image filename.

```bash
IMALYS [tutorial 2e]
home
	directory=»user«/.imalys
	log=»tutorial«/results
compile
	period=20220801-20221031
reduce
	select=compile
	execute=bestof
export
	select=bestof
	target=»tutorial«/results/B234567_20220801-20221031.tif

# »user« must be exchanged with the home directory of the user
# »tutorial«  must be exchanged with the path of the tutorial
# The results of [tutorial 2d](#t2d-import-with-an-image-catalog) must be available at the working directory
```

Tutorial 2e is nearly identical to [tutorial 2b](#t2b-enhance-image-quality)
except the time period under [compile](../process-definitions/compile.md) and
[export](../process-definitions/export.md). The result of T2e is an 6 band image
of the second half of the vegetation period of 2022.
