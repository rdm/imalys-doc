# [T6] Export Formatted Results

Export allows to save internal results to a selected place. Imalys stores the
results of all processes to the working directory. Images are stored in the ENVI
format, vector data and tables use the WKT format. Both formats support fast and
easy processing. During export the data can be translated to a variety of other
formats. The format is controlled by the given extension.

```bash
IMALYS [catalog]
...
export
    select = mapping
    target = »user«/results/fabric_classes.tif
export
    select = index
    target = »user«/results/fabric_zones.shp
```

The [export](../process-definitions/export.md) command depends on the context. It basically adopts the result of the last given command and saves it in the selected format. The export format is controlled by the extension in the result name. [Export](../process-definitions/export.md) can be repeated after each command to save new results.

Special conditions apply to zones, classes and vectors.
