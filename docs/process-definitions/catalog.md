# Catalog

## Store a position and size database for image archives

The ```catalog``` command was implied to select appropriate archives for an [→
import](import.md) process effectively. The ```catalog``` is an attributed point
geometry using the WKT-format. They attributes include the coordinates of the
tile center, the horizontal and vertical dimension of the tile and the filename.
The ```catalog``` can be visualized using a GIS. The default filename is
```tilecenter```.

## Select image archives to be included

```Archives = [string]```

A filename mask must be given mainly to select the directory or the sensor type.
All archives that share the given mask will be accepted. Subdirectories are not
included.

## Select a filename for the database

```Target = [string]```

The ```catalog``` can be named as needed. The default is ```tilecenter``` at the
directory of the archives.
