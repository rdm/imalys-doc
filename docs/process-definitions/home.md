# Home

## Create or select a working directory (mandatory)

Imalys needs the name of a working directory. The name of another directory to
store messages and warnings is recommended. The ```home``` command at the
beginning of each process chain is mandatory.

The first word of the first line of the process chain must be ```IMALYS```. The
remainder of the first line is ignored and can be used for hints.

## Assign or create a working directory (mandatory)

```Directory = [string]```

Imalys uses large matrix transformations. To get sufficient speed, a quick local
memory should be assigned as a working directory. Final results can be stored
with [export](export.md) at another place.

Imalys tries to install a standard working directory ```./imalys``` at the users
home directory. If several process chains are called at the same time, each
chain needs a separate working directory.

## Clear the working directory (option)

```Clear = [true]```

Most processes will produce various intermediate results. If the final result is
stored at a different directory, the working directory should be cleared at the
beginning of each process chain.

## Set a message directory (option)

```Log = [string]```

Imalys reports each command, returns an activity log for all processes and lists
error messages. The error list includes messages from other software called by
Imalys. Messages should not be stored in the working directory. We recommend to
store results and messages at the same place.
