# Mapping

## Classify image features and create image objects

Imalys provides three mapping alternatives. ```Model=Pixel``` will perform a
fully self adjusting classification based on pixel features. ```Model=Zones```
will do the same with zones including the option to use geometric features of
the zones for landscape description. ```Model=Fabric``` combines existing zones
to “objects” of several zones. Objects are defined by the spatial combination of
their zones. In each case the number of resulting classes and a number of
samples during the training process must be given.

The ```Select```, ```Model```, ```Features``` and ```Samples``` commands are
mandatory.

The ```Entropy``` command under Mapping is replaced by ```Entropy``` under
[Features](features.md).

## Mark an image or →Zones to be classified

```Select = [string]```

If multiple images should be classified together, they must be stacked using the
[compile](compile.md) command.

## Normalize all values to the same range

```Equalize = [real]```

If image features with very different value ranges are to be classified, the
value ranges should be harmonized. ```Equalize``` scales all features to the
same value distribution controlled by the deviation coeffitient given with
“number”.

## Classifies spectral combinations

```Model = Pixel```

```Pixel``` selects a pixel-oriented classification of the given image data. The
process uses all bands of the provided image. The process is controlled only by
the number of classes in the result. All bands should have a comparable value
range as calibrated images do.

## Classifies zones by their attributes

```Model = Zones```

The command includes all attributes of the given [zones](zones.md) to the
classification process. Pixel features are ignored. If spectral attributes are
mixed with form and size attributes the value ranges should be ```Equalized```.

The [zones](zones.md) must exist.

## Creates and classifies image objects

```Model = Fabric```

In this context adjacent zones with an individual combination of different
features are called objects. The ```Fabric``` process defines different types of
the zones and combines them to image objects in one run. The object definition
relays only on the borders between different types of zones. Thus objects are
characterized by the spatial pattern of classified zones.

The [zones](zones.md) must exist.

## Sets a large environment for the →Fabric process

```Double = [true]```

```Fabric``` can use a large environment to define image object properties but
will need excessive computation time. The use of this option depends on the
specific situation.

```Double``` only works for the ```Fabric``` process

## Specifies the number of classes or objects to be created

```Features = [number]```

The number of classes should not be too large. Overclassification reduces the
accuracy. We recommend to use two classes per desired land use feature. Imalys
classifies statistically. Coincidence can confuse a statistical analysis.
Sometimes one class more or less can (!) significantly improve the quality.

## Specifies the number of samples to train the classifier.

```Samples = [number]```

To find clusters in the feature space →Mapping uses samples from the image data.
These are selected from the picture at random places and reduced in such a way
that their distances in the feature space are equally distributed. Samples make
the classification much faster than when each pixel or zone has to be evaluated
individually. We recommend to use around 1000 samples per desired class.
