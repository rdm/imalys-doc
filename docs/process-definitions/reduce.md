# Reduce

## Image dimension by pixel based processing

Imalys provides 12 different processes to transfer an image stack to a new image
with a defined number of layers. One source image can be processed to each of
the possible results with the same command. The results will be named as the
process.

## Mark one image from the working directory to be processed

```Select = [string]```

More than one image can be processed at the same time if they are stacked using
the [compile](compile.md) command

## Variance based on standard deviation

```Execute = Variance```

The ```variance``` command determines the variance of individual pixels based on
a standard distribution for all of the bands in the source. For multiple image
stacks, ```variance``` determines the variance for each band separately and
gives the result as a multispectral image of the variances. The result can be
further reduced to a one band image with the first principal component of all
bands using
```[principal](reduce.md#gives-the-first-principal-component-of-all-bands)```.

## Regression based on standard deviation

```Execute = Regression```

```regression``` returns the regression of individual pixels of all bands in
source. ```regression``` uses the temporal distance of the recordings from the
metadata of the images. To do this, the images must have been imported with
[```import```](import.md). Similar to ```variance```, ```regression```
determines the regression for each band of a multiple image stack separately and
gives a multispectral regression.

## Euklidian distance of two (multiband) images

```Execute = Difference```

```difference``` gives the difference between the values of two bands or two
images. For multispectral images ```difference``` generates a result for each
band separately and returns a multispectral image of the differences for all
bands.

## Near Infrared Vegetation Index (NIRv)

```Execute = NirV | NDVI```

Both, the NIRv[^1] and the
[NDVI](https://en.wikipedia.org/wiki/Normalized_difference_vegetation_index)
index are calculated as the product of near infrared radiation and the
normalized difference of the red and the near infrared radiation. The NirV
definition shows a better mapping at sparsely vegetated areas.

Vegetation indices were introduced to estimate the plant covered proportion of
the landscape. The result depends on the photosynthetic activity of the plants.
The NirV index tries to quantify the photosynthetically active radiation (PAR)
as a measure of plant metabolism. The EVI was introduced to give a better There
are about 20 different approximations described².

## Proxy for leaf covering per area

```Execute = LAI```

The ```LAI``` gives the proportion of leaf surface compared to the ground
surface covered by the plants. The LAI was introduced as a proxy for field work.
Simulated LAI values by means of remote sensing are of minor quality.

## First principal component of all bands

```Execute = Principal```

The first principal component reflects the brightness or density of all image
bands. Imalys uses this process as brightness in several other cases.

## Arithmetic mean, bands or images

```Execute = Mean```

```mean``` gives the arithmetic mean of all image bands provided. For multiple
image stacks, ```mean``` is individually calculated for each band. The process
returns a multispectral image of mean values.

## Most common value for each pixel from a stack of bands

```Execute = Median```

The Median reflects the most common value of each pixel in a stack of bands or
images. For multiple image stacks, the ```median``` is individually calculated
for each band.

The ```median``` will mask rare values. Image disturbances like clouds or smoke
will be masked if more than the half of all pixels show a unchanged value. Value
in the middle of a sorted value list.

## Automatically choose the most appropriate generalization

```Execute = BestOf```

Imalys stores a quality indicator as percentage of undisturbed image pixels for
each band or image. This indicator allows to decide wether a single but
undisturbed image, the mean of two images or the median of three or more images
are used to represent the values of a time course or other image combinations.

## Rename the result of the last command

```Target = [string]```

```Target``` renames the result of the last command. ```target`````` only works
in the working directory. Only the last result will be affected. Choose
[export](export.md) to store one or more results at a different place.

If no target is given, the result will be named as the process given by
```execute```.

[^1]: Badgley, G., Field, C.B., Berry, J.A., 2017. Canopy near-infrared
    reflectance and terrestrial photosynthesis. Sci. Adv. 3 (3),
    [https://doi.org/10.1126](https://doi.org/10.1126) sciadv.1602244.