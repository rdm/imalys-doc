# Compare

## Validate and / or assign classes

The self adjusting classification [mapping](mapping.md) is driven by image
features. Real classes are not necessarily defined by their appearance.
```Compare``` allows to evaluate if and up to witch degree real classes can be
detected by image features. The main result is a confusion matrix for false and
true detection and denotation. The result can be used to assign class names and
a confidence level to the statistical outcome of the ```Mapping``` process. The
comparison is done by a rank correlation that will be independent from the value
distribution.

```Compare``` works but is not checked under different conditions

## Selects a class reference (raster or vector)

```Reference = [filename]```

Reference classes are assigned to the [mapping](mapping.md) results by means of
a rank correlation after Spearmann[^1]. A rank correlation is independent of the
basic value distribution. Therefore it can be used for each set of data, even a
mix of form and spectral features.

## Stores a vectorized reference layer to a raster image (option)

```Raster = [true]```

The ```Raster``` option stores the internal class reference to a named raster
image.

## Marks a field in the reference table that contains class names

```Fieldname = [string]```

The option ```Fieldname``` marks of a given column in the reference layer as
class names of the reference.

## Assign class names from a reference classification (option)

```Assign = [true]```

The option ```Assign``` transfers the class names of the reference layer to the
results of the [mapping](mapping.md) process

## Stores additional accuracy tables

```Control = [boolean]```

The standard result of ```compare``` is an accuracy table. Using the
```Control``` option, an image “accuracy” is created, that shows only accurate
classifications. The result shows spatial distribution of the errors.
```Control``` also stores a table “combination” with all links between reference
and classification and table “specifity” with more accuracy measures.

[^1]: Hier fehlt eine Quellenangabe.
