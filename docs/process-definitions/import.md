# Import

*Select, extract, calibrate, clip, warp and scale images.*

```Import``` includes all necessary tools to provide a seamless image for a
selected geometry, projection, time, quality and calibration from a
collection of compressed satellite image archives as they are shipped by the
providers.

## Position database

Archives can be imported using ```database```, ```distance``` and ```period```
to select archives for a given place and time ([atalog](catalog.md)).
Alternatively the archives can be selected by their filenames using
```select```. ```database``` or ```select``` is mandatory. Uncompressed raster
images can be imported with [compile](compile.md).

## Process selection

After decompression, the ```frame``` command cuts the result to the borders of
the ```frame``` polygon. ```quality``` rejecStart with Imalysts images with less than the given
partition of clear pixels within the borders of ```frame```. ```bands``` selects
the given bands. ```factor``` and ```offset``` can be used to calibrate the raw
image values to TOA reflectance [0…1] or radiation in [W/m²]. If the EPSG number
after ```warp``` differs from the given projection the image is reprojected
using the pixel size ```pixel```.

## Results

The result is a list of images that cover at least a part of the ```frame```,
show less than ```quality``` unclear pixels and are projected to a common
raster. The results are named using the sensor type, the tile ID and the date.
All Imalys processes store their results to the working directory.

## Elevation

Apart from reflection, other physical characteristics of the depicted surfaces
can also be detected with remote sensing. Height, slope and exposure of a
surface can be measured from elevation measurements ([elevation](elevation.md)).
Elevation data requires special data sources such as the SRTM or the ASTER
mission.

## Temperature

Landsat includes a sensor that registers the heat radiation of the surface. Heat
radiation from Landsat images can be calibrated with [scaling](scaling.md) to
temperatures in ```°K```.

## Select a [catalog](catalog.md) with image archive properties

```Database = [string]```

```database``` selects an image archives [catalog](catalog.md) to select
appropriate archives. The [catalog](catalog.md) is a point geometry with
parameters that can be controlled using a GIS. To specify the selection at least
a ```distance``` between the center points of the image and the region of
interest and the acquisition time ```period``` must be given. ```database``` or
```select``` is mandatory.

## Select the minimum coverage of the archive (only with “Database”)

```Distance = [real]```

```distance``` controls the maximum distance between the center points of the
selected ```frame``` and the tile in the image archive. The input value is
interpreted as radius of the archived tile. If this tile reaches the center of
the selected image, the ```distance``` is 1.0. In most cases archived the tiles
show a black border so the ```distance``` can be smaller than 1.0. In each case
the selection is refined by the ```quality``` parameter.

## Select a time interval for image acquisition (only with ```Database```)

```Period = [string]```

```period``` allows so select archives of a given time interval. The acquisition
```period``` must be given as to dates formatted as ```YYYYMMDD – YYYYMMDD```
with ```Y=year```, ```M=month``` and ```D=day```. ```period``` relays on the
date of the file name.

## Apply a quality mask to reject images

```Quality = [real]```

Most of the public remote sensing images
are shipped with a quality mask. This mask can be used to cut out image
disturbances. The “quality” process only checks the image quality within the
given “frame” [[see T1](../tutorials/t1-start-with-imalys.md)].

## Select Image archives by a filename mask

```Select = [string]```

```select``` extracts image data from
compressed archives to the working directory. ```select``` must provide the
whole filename of the archive. The process can be repeated as often as
necessary. ```database``` or ```select``` is mandatory.

## Cut out parts of the archived images

```Frame = [string]```

Satellite images are provided in partly
overlapping tiles. A polygon or at least three vector points given as a geometry
file can be used to define a Region Of Interest (ROI). If a ROI is given each
archive image is clipped to this ROI. If the ROI is dissipated over different
tiles, Imalys tries to combine them to one seamless image [[see
T1](../tutorials/t1-start-with-imalys.md)].

## Select ```bands``` to be extracted from the archive.

```Bands = [CSV]```

```Bands``` allows to restrict the extraction to specified bands. The selection
must be given as a comma separated list of band names as the provider calls
them.

## Calibrate raw image data to a defined product

```Factor = [real]```

## ???

```Offset = [real]```

Satellite images are provided with values that support easy storage and
transportation. For image analysis it is strongly recommended to use
atmospherically corrected reflectance (TOA reflection) or radiation. Reflectance
is defined as the proportion between absorbed and reflected light, radiation as
the energy of the reflected light given in [W/m²].

The input is sensor dependent and may change between the different product
levels of the providers. Calibration values are part of the metadata of each
provided image but sometimes they are difficult to find. Examples for most
common sensors are shown at [[T1](../tutorials/t1-start-with-imalys.md)].

## Reproject and scale images

```Warp = [EPSG]```

## ???

```Pixel = [real]```

To use different images of the same region or to adopt images with given maps
the projection of the image can be changed. The process depends on the image
type. Maps and classified images are reprojected using the nearest neighbor
principle. All other images are interpolated by a bicubic convolution process.

```Warp``` expects an EPSG number as new projection. ```pixel``` changes the
pixel size during the reprojection process. ```warp``` and ```pixel``` can be
used also if only the pixel size is to be changed. The values for ```warp``` and
```pixel``` must be always given together.
