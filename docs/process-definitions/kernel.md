# Kernel

## Extract context features of each pixel using a moving window

Kernel processes collect values in the neighborhood of each pixel and combine
them to a new value. The kernel is a small square window that is moved over the
whole image. One source image can be processed with any of the possible kernels
at the same time. The results will be named as the process.

The ```elevation``` process is dedicated for elevation models. ```entropy```,
```deviation``` and ```roughness``` are no longer supported under [kernel](kernel.md). Statistical and even textural indicators should be called
under [features](features.md).

## Mark one image at the working directory to be processed

```Select = [string]```

More than one image can be processed at the same time if they are stacked using
the [```compile```](compile.md) command

# Select one or more of the possible kernel processes.

```Execute```

The results will be named as the commands.

## Select the kernel size

```Execute = Radius```

The kernel ```Radius``` is defined as the number of pixels between the center
and the border of the kernel. The radius “1” produces a 3x3 pixels kernel. Zero
is not accepted.

## Rename the result of the last command

```Execute = Target```

```Target``` renames the result of the last command. ```Target``` only works in
the working directory. Choose [export](export.md) to store one or more results
at a different place.

## Extract the local “diversity” of each pixels environment

```Execute = Texture```

```Texture``` extracts the differences between each pixel and its neighbors
within a small window (kernel) and stores the first principal component of all
differences to a new layer. Landscape diversity increases with texture.

## Extract a normalized “diversity” of each pixels environment

```Execute = Normal```

As ```Texture```, ```Normal``` collects the differences between all pixel
neighbors within a small window but uses the normalized differences instead of
the absolute values. The normalized texture is independent from brightness or
illumination (shadows).

In contrast to Rao’s [Entropy](#raos-diversity-based-on-classes) or the
[Roughness](#raos-diversity-based-on-pixels) command, ```Normal``` will return
similar values for regular and randomly distributed patterns.

## Inverse Difference Moment (IDM)

```Execute = Inverse```

```Inverse``` creates a new image with the Inverse Difference Moment (IDM)
proposed by Haralik⁵

```Inverse``` is particularly high in dark regions and low in bright regions. It
can complement ```texture``` and has proved useful in the analysis of settlement
structures.

## Reduce the local contrast

```Execute = Lowpass```

```Lowpass``` reduces the local contrast of the image data according to the
selected ```Radius```

The process reduces the local contrasts and can fix small bugs. ```Lowpass```
uses a kernel with a normalized Gaussian distribution. The kernel size can be
selected freely. Imalys implements large kernels through an iterative process to
significantly reduce the processing time.

## Enhance the local contrast

```Execute = Laplace```

A ```Laplace``` transformation (Mexican Head function) enhances the image
contrast and can make lines and closed shapes clearly visible. Imalys implements
the transformation as the difference between two Gaussian distributions with a
different radius. The parameters ```inner``` and ```outer``` control the size
(kernel radius) of the two distributions.

## Extract slope, exposition and shade from an elevation model

```Execute = Elevation```

The command generates three layers called ```slope```, ```exposition``` and
```shade``` from any elevation model. The ```hillshade``` command can
superimpose the shading over each other image thus providing a 3D look to each
other result.
