# Export

## layers from the working directory

The result of the process chain can be exported from the working directory to
another place. The image or vector format can be selected. Classified images can
be exported as attributed polygons. In all cases the extension of the target
filename controls the format of the export.

## Select a raster or vector layer to be exported

```Select = [filename]```

Each raster or vector layer at the working directory can be selected. The export
will transform the result according to the file name extension of the
```Target```. A raster to vector conversion will only work if the raster layer
is classified.

[Mapping](mapping.md) will be export the classification result together with the
binary class definition (BIN format). [zones](zones.md) will be exported as
polygons together with a binary attribute table, a binary topology table and the
pure geometry using WKT format.

## Selects the filename and the format of the output file(s)

```Target = [filename]```

Raster based results can be exported to 48 different raster formats. Raster
export includes results from vector based processes. Standard format is ENVI
labelled.

Vector based results can be exported to 23 different vector formats. Vector
export includes automated transformation for classified raster data. Standard
format is ESRI Shape.
