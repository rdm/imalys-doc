# Repeat

## Repeat the process chain with different variable sets

To operate the ```repeat``` mode, ```xImalys``` must be called with two
parameters. The first parameter must be the name of the process chain, the
second a variable list. Imalys will repeat the process chain once again for each
line in the value list (below) without any further interaction.

## Replace variables using a value table

To repeat one process chain with different input sets, the variables must be
given by a text file that contains only the variables. The variables for one run
must be given as a tab separated line. Each line will call a new run and use
only the variables of this line. The columns of the text table represent the
variable number.
