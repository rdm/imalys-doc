# Features

## Create zone attributes from size, shape and pixel statistics

During [zones](zones.md) creation only the borders of the individual areas are
recorded. To characterize the different zones spectral and morphological
attributes can be added to the resulting vector layer. Morphological features
like ```Dendrites``` or ```Proportion``` only relay on the morphological
properties of the zones. ```Select``` will add all spectral features of the
given image and statistical features like “Normal” or “Entropy” integrate both
spectral and morphological information.

Statistical features integrate all spectral bands to one result value. To
integrate all bands appropriately, Imalys first computes the result for each
band separately and returns the first major component of all bands as a result.

```Diffusion``` allows to focus all results to local minima and maxima.

The results of ```features``` are stored in the ```index.bin``` file. If the
vector layer is exported, they show up as attributes of the exported geometry.

## Assign spectral features as zone attributes

```Select = [string]```

```Select``` adds spectral features from all bands of the image [string] to the
attribute table. Each image with the same geometry as the zones image can be
selected. The attribute values are the mean of all pixels within one zone.

## Assign attributes from size, shape and context

```Execute```

Imalys can add form, size, context and pixel features (below) to the attribute
table of the [zones](zones.md). ```Execute``` can be called as often as
necessary with different parameters.

## Quotient of zone perimeter and zone size

```Execute = Dendrites```

```Dendrites``` returns the quotient between perimeter and size of single zones.
Both values grow with larger zones but the size grows faster. Large zones will
show lower values than smaller ones with the same shape.

```Dendrites``` was introduced as a size independent measure of spatial
diversity. Long and small zones may be quite large but their role in landscape
diversity is similar to small zones. Small, thin or dendritic shaped zones show
high values, large compact zones show the lowest.

## Pixel diversity following Rao’s proposal

```Execute = Entropy```

Textural features like the process ```texture``` or ```normal``` return the
“roughness” of an image. Regular pattern will show a high roughness but their
deviation or spectral diversity might be comparably low. Several proposals {==[^1]
[^2]==} to measure “real” diversity share the concept to compare not only
adjacent pixels but all of them within a given region.

The calculation uses the Gaussian formulae for deviation that return the same
result if the image value distribution follows a standard deviation. For optical
images this the case.

[Zones](zones.md) are superior to kernels in determining the local diversity
because they respect natural boundaries. To determine the diversity of larger
regions, whole zones should be compared with the ```deviation``` command. Both
scales can be used together to determine the “real” diversity ({==see ###==}).

## Normalized pixel texture for individual zones

```Execute = Normal```

As the ```Texture``` command does, ```Normal``` returns  the mean difference
between all pixel pairs within an individual zone but in this case the
difference is normalized by the mean brightness of the compared pixels. Texture
in bright regions like industrial areas will be high even if the relative
differences are low. The opposite is true for dark forests. The normalized
texture is independent of the object brightness.

## Pixel texture for individual zones

```Execute = Texture```

The ```texture``` command returns the mean difference between all pixel pairs
within an individual zone. The ```texture``` command thus returns the
“roughness” of the image. The ```normal``` process will return a brightness
independent result.

## Spectral diversity of the central zone and all neighbors

```Execute = Deviation```

The spectral diversity between zones is calculated as the statistical deviation
of all color attributes between the central zone and all its neighbors. As the
zones might differ considerably in size and shape the length of the common
border was selected as a measure for the contribution of the different zones to
the final value. For multispectral images the deviation of the different bands
is calculated independently and the principal component of all deviations is
taken as the final value. The ```Deviation``` command behaves like a small
```Entropy``` kernel using zones instead of pixels.

## Size diversity of the central zone and all neighbors

```Execute = Proportion```

```Proportion``` returns the relation between the size of one zone and all its
neighbors. The result is calculated as difference between the size of the
central zone and the mean size of its neighbors. As the size is given in a
logarithmic scale, the “mean” is not an arithmetic but a geometric mean.

```Proportion``` resembles a texture for zonal sizes. Proportion is independent
of the individual size of a zone. Values around zero indicate equally sized
neighbor zones. Zones with smaller neighbors show positive values, zones with
larger neighbors are negative.

## Quotient of number of neighbors and cell perimeter

```Execute = Relation```

```Relation``` is calculated as the relation between the number of neighbor
zones and the perimeter of the zone.

```Relation``` was introduced as an indicator for spatial diversity. Large and
small zones with comparatively few neighbors will have large values. Small zones
with many neighbors will show the highest values. Like ```Dendrites``` also
```Relation``` returns information about the shape and the connection of the
zones. Zones with many connections may provide paths for animal travels and
enhance diversity.

## Size of each zone given as natural logarithm

```Execute = Size```

The zonal size is calculated from the sum of pixels covering the zone. The
landscape diversity increases with smaller zones

## Smooth features in a network of zones

```Execute = Diffusion```

The algorithm for value equalization in zones mimics diffusion through membranes
(borders). In the process, features “migrate” into the neighboring zone like
soluble substances and combine with existing concentrations. The intensity of
diffusion depends on the length of the common border, the concentration
difference  and the selected number of iterations. The size of the zones does
not matter.

The process is only controlled by the number of iterations. Each iteration
includes a new layer of contributing zones. The influence of distant zones on
the central zone decreases with distance. Entries over 10 are still allowed, but
rarely have a visible effect.

## Raster representation of a vector map with attributes

```Execute = Values```

```Values``` creates a multi band raster image from the vector borders and the
attributes of the different polygons. ```Values``` was introduced mainly as a
control feature.

[^1]: Hier fehlen noch Quellenangaben!

[^2]: Hier fehlen noch Quellenangaben!
