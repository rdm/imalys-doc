# Process Definitions

Imalys uses ```hooks``` with commands and parameters to process image data. Hooks can
contain as many commands as necessary. The following list explains in detail
each command and all possible parameters. Commands are given as a single word in
one line, parameters as an ```name = value``` expression.

- [Replace](replace.md)
- [Repeat](repeat.md)
- [Catalog](catalog.md)
- [Home](home.md)
- [Compile](compile.md)
- [Reduce](reduce.md)
- [Kernel](kernel.md)
- [Zones](zones.md)
- [Features](features.md)
- [Mapping](mapping.md)
- [Compare](compare.md)
- [Export](export.md)
