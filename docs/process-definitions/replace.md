# Replace

## Replace variables given in the process chain

Imalys allows to use variables to manage long process chains and to repeat
process chains with different input data. Variables consists of a ```$``` sign
followed by one number from 1 to 9. Variables can be defined either at the
beginning of the process chain or passed via a second text file to repeat the
process chain with a list of inputs.

## Working directory

The working directory is predefined for each input filename. If only the
filename is given, Imalys expands the filename to the working directory.

## Replace locally defined variables

```$? = [value]``` 

If a process chain should be used for different tasks, local variables can be
used. Imalys will substitute each variable with the assigned value. To recall a
process chain with different parameters only the variables have to be changed.
