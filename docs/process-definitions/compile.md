# Compile

## Select, translate and stack bands or images

The Compile command will stack all selected images to a multi layer image to
support further processing. The compilation ensures that all selected bands
share the same projection, pixel size and frame. If the selected images do not
cover the same region, the missing areas are filled with ```NoData (1/0)```. The
result is translated to the [ENVI format]() and stored to the working directory.

The ```warp``` and the ```pixel``` parameter must be given if the projection of
the selected images differ to define the result.

## Prepare bands or images to be processed (mandatory)

```Select = [string]```

Only selected images will be processed. Projection and pixel size must be the
same or transformed with ```Warp``` and ```Pixel``` to common values. The
coverage of the images can be different. During the stacking process each band
is positioned according to its coordinates. ```Select``` can be repeated as
often as necessary. All selected images will be stacked.

## Select images from a given time period

```Period = [string]```

Like ```Period``` under [import](import.md), ```Period``` needs a time period
given as ```YYYYMMDD-YYYYMMDD``` (Y=Year; M=Month; D=Day). All images within the
given dates will be processed in the same way as if they would be selected
individually.

## Assign a filename for the compiled image stack

```Target = [string]```

```Target``` will rename the result of the ```compile``` process. If no
```target``` is given, the result will be called ```compile```. To translate
images and store them to another directory the [export](export.md) command
should be used. To rename other files in the working directory ```select``` and
```target``` under ```compile``` can be used.
