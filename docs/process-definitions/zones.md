# Zones

## Delineate homogeneous image elements

The ```zones``` process delineates a seamless network of image segments that
completely covers the image. The ```zones``` process always tries to minimize
the pixel diversity within one zone. The process can be controlled by a given
maximum of the diversity within one zone or a given mean size of the resulting
zones. Both controls can be used together.

```zones``` were introduced to provide a structural basis for landscape
diversity and other structural features. ```zones``` allow an easy
transformation of raster images to a vector format like maps. ```zones``` can be
combined by the [Fabric](fabric.md) process to larger “Objects” that are
characterized by their spatial patterns.

```zones``` are stored in an internal format to support rapid processing. Use
the [export](export.md) command to get attributed polygons.

## Mark one image as basis of the →Zones process

```Select = [string]```

The ```zones``` command will work with all image types. Scalar values will be
segmented according to local contrast and size of the result zone. Thematic
images (maps) will be converted into the vector format.

## Select the highest diversity for single zones

```Limit = [float]```

Imalys will always try to combine pixel in a way that will minimize the the
diversity within one zone. The result will consist of zones with different sizes
due to different internal diversity. If lower contrast borders should be catched
or the size of the zoned should be more homogeneous the diversity of single
zones can be limited.

## Select the mean size of the zones

```Size = [number]```

If nor diversity limit is given, Imalys will grow initially created ```zones```
until the product of size and diversity reaches the given value. If no diversity
```limit``` is given, the process delineates ```zones``` with a “natural
looking” distribution of large and small ```zones```. Large ```zones``` show
lower diversity and vice versa.

Diversity relays on a handful of parameters. Appropriate inputs for given images
must be found by experiments.
